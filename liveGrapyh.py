import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import subprocess

find_msg = "Recibi interrupcion SW"
last_dmesg_line = ""

# Se saca del archivo el valor de los 3 pulsadores y se lo guarda en un array
def get_v():
    f = open("/dev/DRIVER_CDD_TP5", "rb")
    lista = f.readline(12)
    f.close()

    switch = lista[0]
    grapha = lista[4]
    graphb = lista[8]

    valores = [switch, grapha, graphb]

    return valores

# Actualizacion del array, elimino valor antiguo, ingreso x
def update(arr,x):
    del arr[0]
    arr.append(x)

# Se grafica los valores del pulsador solicitado
def graficar(pulses, title ,color):
    plt.clf()  # Clear the previous plot
    plt.xlim(0, 10)
    plt.title(title)
    plt.xlabel("Tiempo")
    plt.ylabel("Pulsaciones")
    plt.bar(list(range(10)), pulses, align='center', color=color)
    plt.draw()

# Se crea un array de 10 ceros para los dos posibles valores del switch
signal_0 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
signal_1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

fig, ax = plt.subplots()

def update_plots(frame):
    global last_dmesg_line, find_msg
    try:
        valores = get_v()

        update(signal_0, valores[1])
        update(signal_1, valores[2])

        if valores[0] == 0:
            print(f"Signal 0 tiene el valor: {signal_0}")
            graficar(signal_0, "Signal 0",'red')
        else:
            print(f"Signal 1 tiene el valor: {signal_1}")
            graficar(signal_1, "Signal 1",'blue')
    except IndexError:
        print("Ocurrio en error en la lectura del driver. Seguimos con la siguiente lectura.")

# Animate the plots every 1 second
ani = FuncAnimation(fig, update_plots, interval=1000)

# Show the plot
plt.show()
