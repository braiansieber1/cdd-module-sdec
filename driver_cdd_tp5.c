#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/uaccess.h>  
#include <linux/gpio.h>     
#include <linux/interrupt.h>

#define SWITCH      19
#define SIGNAL_0    18
#define SIGNAL_1    17
#define NUM_SIGNALS 3

/*Estructuras para el montado del modulo*/
dev_t dev_tp5;
static struct class *class_tp5;
static struct cdev cdev_tp5;

unsigned int GPIO_irqNumber[NUM_SIGNALS];

int value[NUM_SIGNALS];

static struct gpio gpio_inputs[] = {
		{ SWITCH, GPIOF_IN, "SWITCH" },
        { SIGNAL_0, GPIOF_IN, "SIGNAL_0" },
        { SIGNAL_1, GPIOF_IN, "SIGNAL_1" },
};
static irqreturn_t gpio_irq_handler_sw(int irq,void *dev_id);
static irqreturn_t gpio_irq_handler_signal_0(int irq,void *dev_id);
static irqreturn_t gpio_irq_handler_signal_1(int irq,void *dev_id);
static int DRIVER_CDD_TP5_open(struct inode *inode, struct file *file);
static int DRIVER_CDD_TP5_release(struct inode *inode, struct file *file);
static ssize_t DRIVER_CDD_TP5_read(struct file *filp, char __user *buf, size_t len, loff_t *off);
static ssize_t DRIVER_CDD_TP5_write(struct file *filp, const char __user *buf, size_t len, loff_t * off);

static struct file_operations fops =
{
    .owner          = THIS_MODULE,
    .read           = DRIVER_CDD_TP5_read,
    .write          = DRIVER_CDD_TP5_write,
    .open           = DRIVER_CDD_TP5_open,
    .release        = DRIVER_CDD_TP5_release,
};

static int __init DRIVER_CDD_TP5_init(void)
{
    printk(KERN_INFO "DRIVER_CDD_TP5: Module Init\n");

    /*Alocamos un rango de numeros de CD (Major, Minor) */
    if((alloc_chrdev_region(&dev_tp5, 0, 1, "DRIVER_CDD_TP5")) <0){
      
        printk(KERN_INFO "DRIVER_CDD_TP5: No se pudo alocar el Major par el CD\n");
        goto r_unreg;
    }
    printk(KERN_INFO "Major = %d Minor = %d.\n",MAJOR(dev_tp5), MINOR(dev_tp5));
 
    /*Iniciamos la estructura cdev con las operaciones*/
    cdev_init(&cdev_tp5,&fops);
 
    /*Agregamos el CD al sistema, asociado al Major obtenido*/
    if((cdev_add(&cdev_tp5,dev_tp5,1)) < 0){
        printk(KERN_INFO "DRIVER_CDD_TP5: No se pudo agregar el CD al sistema\n");
        goto r_del;
    }
 
    /*Creamos la clase*/
    if((class_tp5 = class_create(THIS_MODULE,"DRIVER_CDD_TP5_class")) == NULL){
        printk(KERN_INFO "DRIVER_CDD_TP5: No se pudo crear la clase\n");
        goto r_class;
    }
 
    /*Creamos el dispositivo, le asociamos el Major y la clase creada*/
    if((device_create(class_tp5,NULL,dev_tp5,NULL,"DRIVER_CDD_TP5")) == NULL){
        printk(KERN_INFO "DRIVER_CDD_TP5: No se pudo crear el Device\n");
        goto r_device;
    }

    
    if(gpio_request_array(gpio_inputs, ARRAY_SIZE(gpio_inputs))){
        printk(KERN_INFO "DRIVER_CDD_TP5: Fallo en la solicitud de GPIO\n");
        goto r_gpio;
    }


    GPIO_irqNumber[0] = gpio_to_irq(gpio_inputs[0].gpio);
    if (GPIO_irqNumber[0] < 0){
        printk(KERN_INFO "DRIVER_CDD_TP5: no se pudo asociar irq %d.\n", GPIO_irqNumber[0]);
        goto r_gpio;
	}

    GPIO_irqNumber[1] = gpio_to_irq(gpio_inputs[1].gpio);
    if (GPIO_irqNumber[1] < 0){
        printk(KERN_INFO "DRIVER_CDD_TP5: no se pudo asociar irq %d.\n", GPIO_irqNumber[1]);
        goto r_gpio;
	}

    GPIO_irqNumber[2] = gpio_to_irq(gpio_inputs[2].gpio);
    if (GPIO_irqNumber[2] < 0){
        printk(KERN_INFO "DRIVER_CDD_TP5: no se pudo asociar irq %d.\n", GPIO_irqNumber[2]);
        goto r_gpio;
	}
    
    if (request_irq(GPIO_irqNumber[0], gpio_irq_handler_sw, IRQF_TRIGGER_RISING,"DRIVER_CDD_TP5", NULL)) {                     
        printk(KERN_INFO "DRIVER_CDD_TP5: No se pudo setear el handler de IRQ 0\n");
        goto r_gpio;
    }

    if (request_irq(GPIO_irqNumber[1], gpio_irq_handler_signal_0,IRQF_TRIGGER_RISING, "DRIVER_CDD_TP5",NULL)) {                     
        printk(KERN_INFO "DRIVER_CDD_TP5: No se pudo setear el handler de IRQ 1\n");
        goto r_irq0;
    }

    if (request_irq(GPIO_irqNumber[2], gpio_irq_handler_signal_1,IRQF_TRIGGER_RISING, "DRIVER_CDD_TP5", NULL)) {                     
        printk(KERN_INFO "DRIVER_CDD_TP5: No se pudo setear el handler de IRQ 2\n");
        goto r_irq1;
    }

    /*Iniciamos a 0 el numero de veces que se presiono el boton*/
    value[0] = 0;
    value[1] = 0;
    value[2] = 0;
     
    printk(KERN_INFO "DRIVER_CDD_TP5: Modulo cargado correctamente\n");
    return 0;
    
    r_irq1:
    free_irq(GPIO_irqNumber[1],NULL);
    r_irq0:
    free_irq(GPIO_irqNumber[0],NULL);
    r_gpio:
    gpio_free_array(gpio_inputs, ARRAY_SIZE(gpio_inputs));
    r_device:
    device_destroy(class_tp5,dev_tp5);
    r_class:
    class_destroy(class_tp5);
    r_del:
    cdev_del(&cdev_tp5);
    r_unreg:
    unregister_chrdev_region(dev_tp5,1);
    printk(KERN_INFO "DRIVER_CDD_TP5: Fallo al cargar el modulo\n");
    
    return -1;
}

static irqreturn_t gpio_irq_handler_sw(int irq,void *dev_id){
    printk(KERN_INFO "DRIVER_CDD_TP5 : Interrupcion SWITCH\n");

    if(value [0] == 0){
        value [0] = 1;
    }else{
        value [0] = 0;
    }

    return IRQ_HANDLED;
}

static irqreturn_t gpio_irq_handler_signal_0(int irq,void *dev_id){
    printk(KERN_INFO "DRIVER_CDD_TP5 : Interrupcion SIGNAL_0\n");

    value [1]++;

    return IRQ_HANDLED;
}

static irqreturn_t gpio_irq_handler_signal_1(int irq,void *dev_id){
    printk(KERN_INFO "DRIVER_CDD_TP5 : Interrupcion SIGNAL_1\n");

    value [2]++;

    return IRQ_HANDLED;
}

static int DRIVER_CDD_TP5_open(struct inode *inode, struct file *file){
    printk(KERN_INFO "DRIVER_CDD_TP5 : Open.\n");

    return 0;
}

static int DRIVER_CDD_TP5_release(struct inode *inode, struct file *file){
    printk(KERN_INFO "DRIVER_CDD_TP5 : Close.\n");

    return 0;
}

static ssize_t DRIVER_CDD_TP5_read(struct file *filp, char __user *buf, size_t len, loff_t *off){
    printk(KERN_INFO "DRIVER_CDD_TP5 : Read.\n");
    
    if (copy_to_user(buf, &value , sizeof(int)*3)){
        printk(KERN_INFO "DRIVER_CDD_TP5 : Error en ctu().\n");
        return -EFAULT;
    }
    else{
        printk(KERN_INFO "DRIVER_CDD_TP5 : leyendo: %d, %d, %d.\n", value [0], value [1], value [2]);
        return sizeof(int)*3;
    }
}

static ssize_t DRIVER_CDD_TP5_write(struct file *filp, const char __user *buf, size_t len, loff_t * off){
    printk(KERN_INFO "DRIVER_CDD_TP5 : Write.\n");

    value [1] = 0;
    value [2] = 0;

    return len;
}


static void __exit DRIVER_CDD_TP5_exit(void)
{
    free_irq(GPIO_irqNumber[2],NULL);
    free_irq(GPIO_irqNumber[1],NULL);
    free_irq(GPIO_irqNumber[0],NULL);
    gpio_free_array(gpio_inputs, ARRAY_SIZE(gpio_inputs));
    device_destroy(class_tp5,dev_tp5);
    class_destroy(class_tp5);
    cdev_del(&cdev_tp5);
    unregister_chrdev_region(dev_tp5, 1);
    pr_info("DRIVER_CDD_TP5: Modulo quitado correctamente\n");
}


module_init(DRIVER_CDD_TP5_init);
module_exit(DRIVER_CDD_TP5_exit);


MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Modulo del Trabajo Practico 5 de Sistemas de Computacion");
MODULE_AUTHOR("siscom_group: sudo");
MODULE_VERSION("1");