from subprocess import run
from time import sleep

while True:
    # Cambiamos el valor del puerto 17 a 1 y esperamos un segundo
    run(['gpio', '-g', 'write', '17', '1'])
    sleep(1)
    # Cambiamos el valor del puerto 17 a 0 y esperamos un segundo
    run(['gpio', '-g', 'write', '17', '0'])
    sleep(1)
