from subprocess import run
from time import sleep

while True:
    # Cambiamos el valor del puerto 18 a 1 y esperamos un segundo
    run(['gpio', '-g', 'write', '18', '1'])
    sleep(1)
    # Cambiamos el valor del puerto 18 a 0 y esperamos un segundo
    run(['gpio', '-g', 'write', '18', '0'])
    sleep(1)
